const Router = require('express').Router
const { authGuard } = require('../../utils/passport')
const { getUserData, register, updateUser, deleteUser } = require('./service')

const router = Router()

router.get('/', authGuard, async (req, res) => {
  const user = await getUserData(req.user.id)

  res.json(user)
})

router.post('/', async (req, res) => {
  const { username, password, email, user_role } = req.body

  const registerData = await register({ username, password, email, user_role })

  res.json(registerData)
})

router.put('/', authGuard, async (req, res) => {
  const { username, password, email, organisation_id, user_role } = req.body
  const user = await updateUser(req.user.id, { username, password, email, organisation_id, user_role })

  res.json(user)
})

router.delete('/', authGuard, async (req, res) => {
  await deleteUser(req.user.id)

  res.status(204).end()
})

module.exports = router
