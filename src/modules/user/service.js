const argon2 = require('argon2')
const { default: axios } = require('axios')
const config = require('../../utils/config')
const logger = require('../../utils/logger')
const { ServerError } = require('../../utils/ServerError')
const { generateTokens } = require('../auth/service')

const getUserData = async (id) => {
  const {
    data: { username, email, user_role, organisation_id },
  } = await axios.get(`${config.baseUrl}/${id}`)

  return { username, email, user_role, organisation_id }
}

const register = async ({ username, password, ...params }) => {
  let hashed = undefined
  try {
    hashed = await argon2.hash(password)
  } catch (hashError) {
    logger.error(`Error hashing the password for ${username}`, hashError)
    throw new ServerError('Register error', 500)
  }

  const {
    data: { id, refresh, email, user_role, organisation_id },
  } = await axios.post(`${config.baseUrl}/`, { username, password: hashed, ...params })

  const tokens = await generateTokens({ id, refresh })

  return { username, email, user_role, organisation_id, ...tokens }
}

const updateUser = async (id, params) => {
  const {
    data: { username, email, user_role, organisation_id },
  } = await axios.put(`${config.baseUrl}/${id}`, params)

  return { username, email, user_role, organisation_id }
}

const deleteUser = async (id) => {
  await axios.delete(`${config.baseUrl}/${id}`)
}

module.exports = { getUserData, register, updateUser, deleteUser }
