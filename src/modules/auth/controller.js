const Router = require('express').Router
const { authGuard } = require('../../utils/passport')
const { login, generateTokens, refresh } = require('./service')

const router = Router()

// authorize endpoint
router.get('/', authGuard, (req, res) => {
  const { id, user_role, organisation_id } = req.user
  res.json({ id, user_role, organisation_id })
})

// login endpoint
router.post('/', async (req, res) => {
  const { username, password } = req.body

  const user = await login(username, password)

  res.json(await generateTokens(user))
})

// refresh token endpoint
router.post('/refresh', async (req, res) => {
  const { refreshToken } = req.body

  if (!(req.headers.authorization && req.headers.authorization.startsWith('Bearer '))) {
    throw new ServerError('Authorization Header missing!', 403)
  }
  const token = req.headers.authorization.slice(7)

  const user = await refresh(token, refreshToken)

  res.json(await generateTokens(user))
})

// logout endpoint
router.post('/logout', authGuard, async (req, res) => {
  // Invalidates current refresh token
  await generateTokens(req.user)

  res.status(200).end()
})

module.exports = router
