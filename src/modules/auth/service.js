const argon2 = require('argon2')
const { default: axios } = require('axios')
const jwt = require('jsonwebtoken')
const config = require('../../utils/config')
const logger = require('../../utils/logger')
const { ServerError } = require('../../utils/ServerError')

const options = {
  issuer: config.JWT.issuer,
  audience: config.JWT.audience,
  algorithm: config.JWT.algorithm,
}

const login = async (username, password) => {
  logger.info(`Logging in user ${username}`)

  const { data: user } = await axios.get(`${config.baseUrl}/username/${username}`)

  try {
    if (await argon2.verify(user.password, password)) {
      return user
    } else {
      throw new ServerError('Wrong username or password', 401)
    }
  } catch (passwordError) {
    logger.error('login error:', passwordError)
    throw new ServerError('Login error', 500)
  }
}

const generateTokens = async ({ id, refresh }) => {
  logger.info(`Generate tokens for id ${id}`)

  if (id === undefined || refresh === undefined) throw new ServerError('Cannot generate tokens', 500)

  // Increment refresh every time and update the database
  refresh = refresh + 1

  const token = jwt.sign({ id }, config.JWT.secret, { expiresIn: config.JWT.expireTime, ...options })
  const refreshToken = jwt.sign({ id, refresh }, config.JWT.refreshSecret, {
    expiresIn: config.JWT.refreshTime,
    ...options,
  })

  // ATTENTION: don't wait for refresh update
  await axios.put(`${config.baseUrl}/${id}`, { refresh })

  return { token, refreshToken }
}

const refresh = async (token, refreshToken) => {
  logger.info(`Refreshing token`)

  const { id } = jwt.verify(token, config.JWT.secret, { ignoreExpiration: true, ...options })
  const { data: user } = await axios.get(`${config.baseUrl}/${id}`)
  let payload

  try {
    payload = jwt.verify(refreshToken, config.JWT.refreshSecret, { ignoreExpiration: false, ...options })
  } catch (verifyError) {
    logger.error('refresh error:', e)

    if (verifyError.message === 'jwt expired') {
      throw new ServerError('Expired. Please login again!', 401)
    } else if (verifyError.message === 'jwt malformed') {
      throw new ServerError('Token is malformed!', 401)
    }
    throw new ServerError('Token is compromised!', 401)
  }

  if (user.id !== payload.id || user.refresh !== payload.refresh) {
    throw new ServerError('Refresh token invalid', 401)
  }

  return user
}

module.exports = { login, generateTokens, refresh }
