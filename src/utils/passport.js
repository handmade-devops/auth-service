const axios = require('axios').default
const passport = require('passport')
const { ExtractJwt, Strategy: JwtStrategy } = require('passport-jwt')
const config = require('./config')

const setupPassport = (passport) => {
  const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.JWT.secret,
    issuer: config.JWT.issuer,
    audience: config.JWT.audience,
    algorithms: config.JWT.alogrithm,
  }

  passport.use(
    new JwtStrategy(options, async (jwt_payload, done) => {
      try {
        const { data: user } = await axios.get(`${config.baseUrl}/${jwt_payload.id}`)
        done(null, user)
      } catch (e) {
        done(e, false)
      }
    }),
  )
}

const authGuard = passport.authenticate('jwt', { session: false })

module.exports = { setupPassport, authGuard }
