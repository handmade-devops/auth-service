const { getSecret } = require('docker-secret')

const DEV = 'development'

const PORT = process.env.PORT || 8080
const NODE_ENV = process.env.NODE_ENV || DEV
const DATA_SERVICE = process.env.DATA_SERVICE || ''
const baseUrl = `http://${DATA_SERVICE}/api/user`

const JWT = {
  issuer: process.env.JWT_ISSUER || 'handmadeDevops',
  audience: process.env.JWT_AUDIENCE || 'handmade.example.com',
  algorithm: process.env.JWT_ALGO || 'HS512',
  expireTime: process.env.JWT_EXPIRE_TIME || '1h',
  refreshTime: process.env.JWT_REFRESH_TIME || '7d',
  secret: NODE_ENV === DEV ? process.env.JWT_SECRET : getSecret(process.env.JWT_SECRET_FILE),
  refreshSecret: NODE_ENV === DEV ? process.env.JWT_REFRESH_SECRET : getSecret(process.env.JWT_REFRESH_SECRET_FILE),
}

module.exports = {
  PORT,
  NODE_ENV,
  DATA_SERVICE,
  baseUrl,
  JWT,
}
