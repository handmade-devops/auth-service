const Router = require('express').Router()

const userController = require('./modules/user/controller')
Router.use('/user', userController)

const authController = require('./modules/auth/controller')
Router.use('/auth', authController)

module.exports = Router
