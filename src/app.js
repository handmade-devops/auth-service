const express = require('express')
const path = require('path')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require('morgan')
const passport = require('passport')
const OpenApiValidator = require('express-openapi-validator')

// Local imports
const config = require('./utils/config')
const logger = require('./utils/logger')
const middleware = require('./utils/middleware')
const { setupPassport } = require('./utils/passport')

// Manage async errors
require('express-async-errors')

// Starting app
const routes = require('./routes')
const app = express()

logger.info(`Connecting to ${config.PORT}`)

/****** Middleware ******/

// Base middleware
app.use(helmet())
app.use(cors())
app.use(express.json())
app.use(
    morgan(`:remote-addr - :remote-user [:date[web]] ':method :url HTTP/:http-version' :status :res[content-length]`),
)

// OpenAPI middleware
const apiSpec = path.join(__dirname, 'api', 'openapi.yml')
app.use(
  OpenApiValidator.middleware({
    apiSpec,
    validateRequests: true,
    validateResponses: {
      removeAdditional: 'failing',
    },
  }),
)

// Passport setup
app.use(passport.initialize())
setupPassport(passport)

// Use routed endpoints
app.use('/api', routes)

// Heathcheck endpoint
app.get('/liveliness', (_, res) => {
    res.status(200).end()
})

// Error handler custom middleware
app.use(middleware.errorHandler)

module.exports = app
