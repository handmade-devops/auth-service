# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/handmade-devops/auth-service/compare/v1.0.1...v1.1.0) (2021-03-12)


### Features

* :star2: added Docker ([51f1fa5](https://gitlab.com/handmade-devops/auth-service/commit/51f1fa50ea536038056c640b5c4f80e90f5e2504))
* added Express app ([6a11248](https://gitlab.com/handmade-devops/auth-service/commit/6a11248cf4fbce244da8f09df3d434179f8c5cce))

### 1.0.1 (2021-03-11)


### Bug Fixes

* :pencil: fixed license string ([1a90831](https://gitlab.com/handmade-devops/auth-service/commit/1a9083189976413aa9b6161281851a51739e6648))

## 1.0.0 (2021-03-11)
